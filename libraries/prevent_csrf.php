<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	namespace Banshee;

	class prevent_CSRF {
		const TOKEN_KEY = "banshee_csrf";

		private $page = null;
		private $user = null;
		private $view = null;

		/* Constructor
		 *
		 * INPUT:  object page, object user, object view
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($page, $user, $view) {
			$this->page = $page;
			$this->user = $user;
			$this->view = $view;
		}

		/* Prevent CSRF attack
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function execute() {
			if ($this->page->module == "setup") {
				return false;
			}

			if (isset($_SESSION[self::TOKEN_KEY]) == false) {
				$_SESSION[self::TOKEN_KEY] = random_string(32);
			}

			$token = hash("sha256", $_SESSION[self::TOKEN_KEY].$this->user->username);

			$this->view->add_javascript("banshee/prevent_csrf.js");
			$this->view->run_javascript("prevent_csrf('".self::TOKEN_KEY."', '".$token."')");

			if (($_SERVER["REQUEST_METHOD"] != "POST") || $this->page->ajax_request) {
				return;
			}

			if ($_POST[self::TOKEN_KEY] == $token) {
				return;
			}

			/* CSRF attack detected
			 */
			if (isset($_SERVER["HTTP_ORIGIN"])) {
				$referer = $_SERVER["HTTP_ORIGIN"];
			} else if (isset($_SERVER["HTTP_REFERER"])) {
				$referer = $_SERVER["HTTP_REFERER"];
			} else {
				$referer = "previous visited website";
			}

			$message = "CSRF attempt via %s blocked";
			$this->view->add_system_warning($message, $referer);
			$this->user->log_action($message, $referer);
			$this->user->logout();

			$_SERVER["REQUEST_METHOD"] = "GET";
			$_GET = array();
			$_POST = array();
		}
	}
?>
