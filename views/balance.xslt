<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Sheet template
//
//-->
<xsl:template match="sheet">
<div class="scroll">
<table class="table table-condensed table-striped table-xs">
<thead>
<tr>
<th>Title</th><th>Amount</th><th>Payed by</th>
<xsl:for-each select="users/user">
<th><div><xsl:value-of select="." /></div></th>
</xsl:for-each>
</tr>
</thead>
<tbody>
</tbody>
<xsl:for-each select="payments/payment">
<tr>
<td><span class="header">Title:</span><xsl:value-of select="title" /></td>
<td><span class="header">Amount:</span><xsl:value-of select="../@currency" disable-output-escaping="yes" /><xsl:value-of select="amount" /></td>
<td><span class="header">Payed by:</span><xsl:value-of select="fullname" /></td>
<xsl:for-each select="parts/part">
<xsl:variable name="position" select="position()" />
<td class="{@type}">
<xsl:for-each select="../../../../users/user">
<xsl:if test="position()=$position"><span class="header"><xsl:value-of select="." />:</span></xsl:if>
</xsl:for-each>
<xsl:value-of select="../../../@currency" disable-output-escaping="yes" /><xsl:value-of select="." />
</td>
</xsl:for-each>
</tr>
</xsl:for-each>
<tfoot>
<tr>
<td>Total:</td>
<td><span class="header">Amount:</span><xsl:value-of select="payments/@currency" disable-output-escaping="yes" /><xsl:value-of select="total/@amount" /></td>
<td></td>
<xsl:for-each select="total/amount">
<xsl:variable name="position" select="position()" />
<td class="{@type}">
<xsl:for-each select="../../users/user">
<xsl:if test="position()=$position"><span class="header"><xsl:value-of select="." />:</span></xsl:if>
</xsl:for-each>
<xsl:value-of select="../../payments/@currency" disable-output-escaping="yes" /><xsl:value-of select="." />
</td>
</xsl:for-each>
</tr>
</tfoot>
</table>
</div>

<div class="btn-group left">
<xsl:if test="@locked='yes'">
<xsl:if test="@edit_ss='yes'">
<a href="/sheet/{@id}" class="btn btn-default">Edit sheet settings</a>
</xsl:if>
<a href="/balance/{@id}/export" class="btn btn-default">Export in CSV</a>
</xsl:if>
<a href="/{@previous}" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/payment.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="sheet" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
