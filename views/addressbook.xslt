<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />

<table class="table table-condensed table-striped">
<thead>
<tr><th>Name</th><th>E-mail address</th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="addressbook/address">
<xsl:sort select="fullname" />
<tr>
<td><xsl:value-of select="fullname" /></td>
<td><xsl:value-of select="email" /></td>
<td><xsl:if test="addressbook_id"><form method="post" action="/{/output/page}"><input type="hidden" name="id" value="{addressbook_id}" /><input type="submit" name="submit_button" value="delete" class="btn btn-default btn-xs" onClick="javascript:confirm('DELETE: Are you sure?')"/></form></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<form method="post" action="/{/output/page}">
<div class="input-group">
<input type="input" name="address" value="{../address}" placeholder="E-mail address" class="form-control" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Add address" class="btn btn-default" /></span>
</div>
</form>

<div class="btn-group">
<a href="/" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/addressbook.png" class="title_icon" />
<h1>Addressbook</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
