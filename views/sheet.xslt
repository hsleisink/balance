<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover">
<thead>
<tr>
<th>Title</th><th>Date</th><th>Sheet owner</th><th>Locked</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="sheets/sheet">
<xsl:variable name="page"><xsl:choose>
<xsl:when test="locked='no'">payment</xsl:when>
<xsl:otherwise>balance</xsl:otherwise>
</xsl:choose></xsl:variable>
<tr class="click" onClick="javascript:document.location='/{$page}/{@id}'">
<td><xsl:value-of select="title" /></td>
<td><xsl:value-of select="date" /></td>
<td><xsl:value-of select="fullname" /></td>
<td><xsl:value-of select="locked" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New sheet</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="sheet/@id">
<input type="hidden" name="id" value="{sheet/@id}" />
</xsl:if>

<label for="title">Title:</label>
<input type="text" id="title" name="title" value="{sheet/title}" class="form-control" />
<label for="date">Date:</label>
<input type="text" id="date" name="date" value="{sheet/date}" class="form-control datepicker" />
<label for="currency">Currency:</label>
<input type="text" id="currency" name="currency" value="{sheet/currency}" class="form-control" />
<label for="participants">Participants:</label>
<span class="selections">
<input type="button" value="all" class="btn btn-default btn-xs" onClick="javascript:$('div#participants input[type=checkbox]').prop('checked', true)" />
<input type="button" value="none" class="btn btn-default btn-xs" onClick="javascript:$('div#participants input[type=checkbox]').prop('checked', false)" />
<input type="button" value="inverse" class="btn btn-default btn-xs" onClick="javascript:$('div#participants input[type=checkbox]').trigger('click')" />
</span>
<div class="row users">
<xsl:for-each select="users/user">
<xsl:sort select="." />
<div id="participants" class="col-xs-12 col-sm-6 col-md-4 col-lg-3"><input type="checkbox" name="participants[]" value="{@id}"><xsl:if test="@selected='yes'"><xsl:attribute name="checked" select="'checked'" /></xsl:if></input><xsl:value-of select="." /></div>
</xsl:for-each>
</div>
<xsl:if test="sheet/@id">
<div><label for="locked">Locked:</label>
<input type="checkbox" id="locked" name="locked"><xsl:if test="sheet/locked='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></div>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save sheet" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="sheet/@id">
<input type="submit" name="submit_button" value="Delete sheet" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>

<div id="help">
<p>You can add users from other groups to this balance sheet by entering their e-mail address in the 'Search for new user' search form.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/sheet.png" class="title_icon" />
<h1>Balance Sheets</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

 </xsl:stylesheet>
