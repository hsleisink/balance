<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<div class="scroll">
<table class="table table-condensed table-striped table-hover">
<thead>
<tr>
<th>Title</th><th>Amount</th><th>Payed by</th>
<xsl:for-each select="users/user">
<th><div><xsl:value-of select="." /></div></th>
</xsl:for-each>
</tr>
</thead>
<tbody>
<xsl:for-each select="payments/payment">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{../../@sheet_id}/{@id}'">
<td><xsl:value-of select="title" /></td>
<td><xsl:value-of select="../../@currency" disable-output-escaping="yes" /><xsl:value-of select="amount" /></td>
<td><xsl:value-of select="fullname" /></td>
<xsl:for-each select="parts/part">
<td><xsl:value-of select="." /></td>
</xsl:for-each>
</tr>
</xsl:for-each>
</tbody>
<tfoot>
<tr>
<td>Total:</td>
<td>&#8364;<xsl:value-of select="payments/@total" /></td>
<td></td>
<xsl:for-each select="users/user">
<td></td>
</xsl:for-each>
</tr>
</tfoot>
</table>
</div>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group left">
<a href="/{/output/page}/{@sheet_id}/new" class="btn btn-default">New payment</a>
<xsl:if test="@edit_ss='yes'">
<a href="/sheet/{@sheet_id}" class="btn btn-default">Edit sheet settings</a>
</xsl:if>
<a href="/balance/{@sheet_id}" class="btn btn-default">View interim balance</a>
<a href="/sheet" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}/{@sheet_id}" method="post">
<xsl:if test="payment/@id">
<input type="hidden" name="id" value="{payment/@id}" />
</xsl:if>
<xsl:if test="payment/payer_id">
<input type="hidden" name="payer_id" value="{payment/payer_id}" />
</xsl:if>

<label for="title">Title:</label>
<input type="text" id="title" name="title" value="{payment/title}" maxlength="32" class="form-control"><xsl:if test="payment/@id and payment/payer_id!=/output/user/@id"><xsl:attribute name="disabled" select="'disabled'"/></xsl:if></input>
<label for="amount">Amount:</label>
<div class="input-group">
<span class="input-group-addon"><xsl:value-of disable-output-escaping="yes" select="@currency" /></span>
<input type="text" id="amount" name="amount" value="{payment/amount}" class="form-control"><xsl:if test="payment/@id and payment/payer_id!=/output/user/@id"><xsl:attribute name="disabled" select="'disabled'"/></xsl:if></input>
</div>
<xsl:for-each select="users/user">
<xsl:variable name="part" select="@part" />
<xsl:if test="not(../../payment/@id) or ../../payment/payer_id=/output/user/@id or @id=/output/user/@id">
<label for="user-{@id}"><xsl:value-of select="." />:</label>
<select id="user-{@id}" name="part[{@id}]" class="form-control">
<xsl:for-each select="../../part/value">
<option value="{.}"><xsl:if test=".=$part"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</xsl:if>
</xsl:for-each>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save payment" class="btn btn-default" />
<a href="/{/output/page}/{@sheet_id}" class="btn btn-default">Cancel</a>
<xsl:if test="payment/@id and payment/payer_id=/output/user/@id">
<input type="submit" name="submit_button" value="Delete payment" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/payment.png" class="title_icon" />
<h1><xsl:value-of select="title" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
