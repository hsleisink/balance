<?php
	class addressbook_model extends Banshee\model {
		private $columns = array();

		public function get_addressbook() {
			$query = "select id, fullname, email from users where organisation_id=%d and id!=%d and status!=%d";
			if (($users = $this->db->execute($query, $this->user->organisation_id, $this->user->id, USER_STATUS_DISABLED)) === false) {
				return false;
			}

			$query = "select u.id, a.id as addressbook_id, u.fullname, u.email from users u, addressbook a ".
			         "where u.id=a.member_id and a.user_id=%d";
			if (($addressbook = $this->db->execute($query, $this->user->id)) === false) {
				return false;
			}

			return array_merge($users, $addressbook);
		}

		public function add_address($address) {
			if (($member = $this->db->entry("users", $address, "email")) == false) {
				$this->view->add_message("E-mail address not found.");
				return false;
			}

			/* Check in organisation
			 */
			if ($member["organisation_id"] == $this->user->organisation_id) {
				$this->view->add_message("User already in group.");
				return false;
			}

			/* Check already in addresbook
			 */
			$query = "select count(*) as count from addressbook where user_id=%d and member_id=%d";
			if (($result = $this->db->execute($query, $this->user->id, $member["id"])) == false) {
				$this->view->add_message("Database error.");
				return false;
			}

			if ($result[0]["count"] > 0) {
				$this->view->add_message("User already in addressbook.");
				return false;
			}

			$keys = array("id", "user_id", "member_id");

			$addressbook["id"] = null;
			$addressbook["user_id"] = $this->user->id;
			$addressbook["member_id"] = (int)$member["id"];

			if ($this->db->insert("addressbook", $addressbook, $keys) == false) {
				$this->view->add_message("Database error.");
				return false;
			}

			return true;
		}

		public function delete_address($addressbook_id) {
			$query = "delete from addressbook where id=%d and user_id=%d";
			return $this->db->query($query, $addressbook_id, $this->user->id) !== false;
		}
	}
?>
