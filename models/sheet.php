<?php
	class sheet_model extends Banshee\model {
		public function count_sheets() {
			$query = "select count(*) as count from sheets s, participants p ".
			         "where s.id=p.sheet_id and p.user_id=%d";

			if (($result = $this->db->execute($query, $this->user->id)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_users() {
			static $result = null;

			if ($result === null) {
				$result = $this->borrow("addressbook")->get_addressbook();
			}

			return $result;
		}

		public function get_sheets($offset, $limit) {
			$query = "select s.*, u.fullname from sheets s, participants p, users u ".
			         "where s.id=p.sheet_id and p.user_id=%d and s.user_id=u.id ".
			         "order by locked, date desc, title limit %d,%d";

			return $this->db->execute($query, $this->user->id, $offset, $limit);
		}

		public function get_sheet($sheet_id) {
			if (($sheet = $this->db->entry("sheets", $sheet_id)) == false) {
				return false;
			}

			$query = "select * from participants where sheet_id=%d";
			if (($participants = $this->db->execute($query, $sheet_id, $this->user->id)) === false) {
				return false;
			}

			$sheet["participants"] = array();
			foreach ($participants as $participant) {
				array_push($sheet["participants"], (int)$participant["user_id"]);
			}


			if (in_array($this->user->id, $sheet["participants"]) == false) {
				$this->user->log_action("unauthorized access request for sheet %d", $sheet_id);
				return false;
			}

			return $sheet;
		}

		public function fix_participants(&$sheet) {
			if (is_array($sheet["participants"]) == false) {
				$sheet["participants"] = array();
			}

			if (in_array($this->user->id, $sheet["participants"]) == false) {
				array_push($sheet["participants"], $this->user->id);
			}
		}

		public function save_oke($sheet) {
			$result = true;

			if (isset($sheet["id"]) == false) {
				if (is_array($sheet["participants"]) == false) {
					$this->view->add_message("Select at least one participant.");
					return false;
				} else if (count($sheet["participants"]) < 2) {
					$this->view->add_message("Select at least one participant.");
					return false;
				}
			} else {
				if (($current = $this->get_sheet($sheet["id"])) == false) {
					return false;
				}
				if ($current["user_id"] != $this->user->id) {
					$this->view->add_message("You are not allowed to change this sheet.");
					$this->user->log_action("unauthorized update request for sheet %d", $sheet["id"]);
					return false;
				}
			}

			if (strlen($sheet["currency"]) > 3) {
				$this->view->add_message("Invalid currency.");
				return false;
			}

			if (trim($sheet["title"]) == "") {
				$this->view->add_message("Specify the title.");
				return false;
			}

			return $result;
		}

		public function create_sheet($sheet) {
			$keys = array("id", "user_id", "title", "date", "currency", "locked");

			$sheet["id"] = null;
			$sheet["user_id"] = $this->user->id;
			$sheet["date"] = date("Y-m-d");
			$sheet["locked"] = NO;

			$this->db->query("begin");

			if ($this->db->insert("sheets", $sheet, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			$data = array("sheet_id" => $this->db->last_insert_id);
			foreach (array_unique($sheet["participants"]) as $user_id) {
				$data["user_id"] = (int)$user_id;
				if ($this->db->insert("participants", $data) == false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function update_sheet($sheet) {
			$keys = array("title", "date", "currency", "locked");

			$sheet["locked"] = is_true($sheet["locked"]) ? YES : NO;

			$this->db->query("begin");

			if ($this->db->update("sheets", $sheet["id"], $sheet, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			if (($current = $this->get_sheet($sheet["id"])) === false) {	
				return false;
			}

			/* Adding users
			 */
			$data = array("sheet_id" => (int)$sheet["id"]);
			foreach (array_unique($sheet["participants"]) as $user_id) {
				if (in_array($user_id, $current["participants"]) == false) {
					$data["user_id"] = (int)$user_id;
					if ($this->db->insert("participants", $data) == false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			/* Removing users
			 */
			foreach ($current["participants"] as $user_id) {
				if (in_array($user_id, $sheet["participants"]) == false) {
					$queries = array(
						"delete from parts where payment_id in (select id from payments where sheet_id=%d and payer_id=%d)",
						"delete from payments where sheet_id=%d and payer_id=%d",
						"delete from participants where sheet_id=%d and user_id=%d");
					foreach ($queries as $query) {
						if ($this->db->query($query, $sheet["id"], $user_id) == false) {
							$this->db->query("rollback");
							return false;
						}
					}
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($sheet) {
			$result = true;

			if ($this->get_sheet($sheet["id"]) == false) {
				return false;
			}

			return $result;
		}

		public function delete_sheet($sheet_id) {
			$queries = array(
				array("delete from parts where payment_id in (select id from payments where sheet_id=%d)", $sheet_id),
				array("delete from payments where sheet_id=%d", $sheet_id),
				array("delete from participants where sheet_id=%d", $sheet_id),
				array("delete from sheets where id=%d", $sheet_id));

			return $this->db->transaction($queries);
		}

		public function notify_users($sheet) {
			$notification = new Banshee\email("New balance sheet", $this->settings->webmaster_email);
			$notification->message(file_get_contents("../extra/sheet_created.txt"));

			foreach ($sheet["participants"] as $user_id) {
				if (($user = $this->db->entry("users", $user_id)) === false) {
					continue;
				}

				$notification->set_message_fields(array(
					"NAME"    => $user["fullname"],
					"TITLE"   => $sheet["title"],
					"WEBSITE" => $this->settings->head_description,
					"URL"     => sprintf("%s://%s/", $_SERVER["HTTP_SCHEME"], $_SERVER["SERVER_NAME"])));
				$notification->send($user["email"], $user["fullname"]);
			}
		}
	}
?>
