<?php
	class balance_model extends Banshee\model {
		public function get_sheet($sheet_id) {
			return $this->borrow("sheet")->get_sheet($sheet_id);
		}

		public function get_participants($sheet_id) {
			return $this->borrow("payment")->get_participants($sheet_id);
		}

		public function get_payments($sheet_id) {
			return $this->borrow("payment")->get_payments($sheet_id);
		}
	}
?>
