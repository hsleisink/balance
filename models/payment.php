<?php
	class payment_model extends Banshee\model {
		public function get_sheet($sheet_id) {
			return $this->borrow("sheet")->get_sheet($sheet_id);
		}

		public function get_participants($sheet_id) {
			static $result = null;

			if ($result === null) {
				$query = "select * from users u, participants p ".
				         "where u.id=p.user_id and p.sheet_id=%d order by fullname";
				$result = $this->db->execute($query, $sheet_id);
			}

			return $result;
		}

		public function get_payments($sheet_id) {
			$query = "select p.*, u.id as user_id, u.fullname from payments p, users u ".
			         "where p.payer_id=u.id and sheet_id=%d order by p.id";

			if (($payments = $this->db->execute($query, $sheet_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($payments as $payment) {
				$result[(int)$payment["id"]] = $payment;
			}

			$query = "select * from parts where payment_id in (select id from payments where sheet_id=%d)";
			if (($parts = $this->db->execute($query, $sheet_id)) === false) {
				return false;
			}

			foreach ($parts as $part) {
				$payment_id = (int)$part["payment_id"];
				$user_id = (int)$part["user_id"];

				if (is_array($result[$payment_id]["parts"] ?? null) == false) {
					$result[$payment_id]["parts"] = array();
				}
				$result[$payment_id]["parts"][$user_id] = (int)$part["part"];
			}

			return $result;
		}

		public function get_payment($sheet_id, $payment_id) {
			static $payments = array();

			$key = sprintf("%d-%d", $sheet_id, $payment_id);

			if ($payments[$key] === null) {
				$query = "select * from payments where id=%d";
				if (($result = $this->db->execute($query, $payment_id)) == false) {
					return false;
				}
				$payment = $result[0];

				if ($payment["sheet_id"] != $sheet_id) {
					$this->user->log_action("unauthorized access request for payment %d in sheet %d", $payment_id, $sheet_id);
					return false;
				}

				$query = "select * from parts where payment_id=%d";
				if (($parts = $this->db->execute($query, $payment_id)) === false) {
					return false;
				}

				$payment["part"] = array();
				foreach ($parts as $part) {
					$payment["part"][$part["user_id"]] = (int)$part["part"];
				}

				$payments[$key] = $payment;
			}

			return $payments[$key];
		}

		private function valid_amount($amount) {
			if ($amount[0] == "-") {
				$amount = substr($amount, 1);
			}

			$parts = explode(".", $amount);
			$count = count($parts);

			if ($count == 2) {
				if (strlen($parts[1]) != 2) {
					return false;
				}
			} else if ($count != 1) {
				return false;
			}

			if (strlen($parts[0]) > 8) {
				return false;
			}

			foreach ($parts as $part) {
				if (valid_input($part, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
					return false;
				}
			}

			if ($amount == 0) {
				return false;
			}

			return true;
		}

		public function save_oke($sheet_id, $payment) {
			$result = true;

			if (isset($payment["id"])) {
				if (($current = $this->get_payment($sheet_id, $payment["id"])) == false) {
					$this->view->add_message("You are not allowed to change this payment.");
					$this->user->log_action("unauthorized update request for payment %d in sheet %d", $payment["id"], $sheet_id);
					return false;
				}

				if ($current["payer_id"] != $this->user->id) {
					return true;
				}
			}

			if (trim($payment["title"]) == "") {
				$this->view->add_message("Specify the title.");
				$result = false;
			}

			if ($this->valid_amount($payment["amount"]) == false) {
				$this->view->add_message("Specify a valid amount.");
				$result = false;
			}

			if (is_array($payment["part"]) == false) {
				$result = false;
			} else {
				if (($users = $this->get_participants($sheet_id)) === false) {
					return false;
				}
				$user_ids = array();
				foreach ($users as $user) {
					array_push($user_ids, (int)$user["id"]);
				}

				$total = 0;
				foreach ($payment["part"] as $user_id => $part) {
					if (in_array($user_id, $user_ids) == false) {
						return false;
					}
					$total += $part;
				}

				if ($total == 0) {
					$this->view->add_message("Nobody shares in this payment.");
					$result = false;
				}
			}

			return $result;
		}

		private function add_payment_parts($payment_id, $parts) {
			foreach ($parts as $user_id => $part) {
				if ($part == 0) {
					continue;
				}

				$values = array(
					"payment_id" => $payment_id,
					"user_id"    => $user_id,
					"part"       => $part);
				if ($this->db->insert("parts", $values) === false) {
					return false;
				}
			}

			return true;
		}

		public function create_payment($sheet_id, $payment) {
			$keys = array("id", "sheet_id", "payer_id", "title", "amount");

			$payment["id"] = null;
			$payment["sheet_id"] = $sheet_id;
			$payment["payer_id"] = $this->user->id;

			$this->db->query("begin");

			if ($this->db->insert("payments", $payment, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->add_payment_parts($this->db->last_insert_id, $payment["part"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function update_payment($sheet_id, $payment) {
			$keys = array("title", "amount");

			if (($current = $this->get_payment($sheet_id, $payment["id"])) === false) {
				return false;
			}

			$this->db->query("begin");

			if ($current["payer_id"] == $this->user->id) {
				if ($this->db->update("payments", $payment["id"], $payment, $keys) === false) {
					$this->db->query("rollback");
					return false;
				}

				if ($this->db->query("delete from parts where payment_id=%d", $payment["id"]) === false) {
					$this->db->query("rollback");
					return false;
				}

				if ($this->add_payment_parts($payment["id"], $payment["part"]) == false) {
					$this->db->query("rollback");
					return false;
				}
			} else {
				$query = "delete from parts where payment_id=%d and user_id=%d";
				if ($this->db->query($query, $payment["id"], $this->user->id) === false) {
					$this->db->query("rollback");
					return false;
				}

				$data = array($this->user->id => $payment["part"][$this->user->id]);
				if ($this->add_payment_parts($payment["id"], $data) == false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($sheet_id, $payment_id) {
			if ($this->get_payment($sheet_id, $payment_id) == false) {
				$this->view->add_message("You are not allowed to delete this payment.");
				return false;
			}

			return true;
		}

		public function delete_payment($payment_id) {
			$queries = array(
				array("delete from parts where payment_id=%d", $payment_id),
				array("delete from payments where id=%d", $payment_id));

			return $this->db->transaction($queries);
		}
	}
?>
