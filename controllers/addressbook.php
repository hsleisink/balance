<?php
	class addressbook_controller extends Banshee\controller {
		private function show_overview() {
			if (($addressbooks = $this->model->get_addressbook()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview", array("search" => $_SESSION["addressbook_search"] ?? null));

			$this->view->open_tag("addressbook");
			foreach ($addressbooks as $address) {
				$this->view->record($address, "address");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Add address") {
					/* Add address
					 */
					if ($this->model->add_address($_POST["address"]) === false) {
						$this->view->add_tag("address", $_POST["address"]);
					} else {
						$this->user->log_action("address %s added to addressbook", $_POST["address"]);
					}
				} else if ($_POST["submit_button"] == "delete") {
					/* Delete address
					 */
					if ($this->model->delete_address($_POST["id"]) === false) {
						$this->view->add_message("Error deleting address from addresbook.");
					} else {
						$this->user->log_action("user %d deleted from addressbook", $_POST["id"]);
					}
				}
			}

			/* Show overview
			 */
			$this->show_overview();
		}
	}
?>
