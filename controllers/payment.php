<?php
	class payment_controller extends Banshee\controller {
		private function show_overview($sheet) {
			if (($payments = $this->model->get_payments($sheet["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($users = $this->model->get_participants($sheet["id"])) == false) {
				$this->view->add_tag("result", "Error getting users.");
				return false;
			}

			$args = array(
				"sheet_id" => $sheet["id"],
				"edit_ss"  => show_boolean($sheet["user_id"] == $this->user->id),
				"currency" => $sheet["currency"]);
			$this->view->open_tag("overview", $args);

			$this->view->open_tag("users");
			foreach ($users as $user) {
				$this->view->add_tag("user", $user["fullname"]);
			}
			$this->view->close_tag();

			$total = 0;
			foreach ($payments as $payment) {
				$total += $payment["amount"];
			}

			$this->view->open_tag("payments", array("total" => sprintf("%0.2f", $total)));
			foreach ($payments as $payment) {
				$this->view->open_tag("payment", array("id" => $payment["id"]));

				$this->view->record($payment);

				$this->view->open_tag("parts");
				foreach ($users as $user) {
					$this->view->add_tag("part", $payment["parts"][$user["id"]] + 0);
				}
				$this->view->close_tag();

				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_payment_form($sheet_id, $payment) {
			if (($users = $this->model->get_participants($sheet_id)) == false) {
				$this->view->add_tag("result", "Error getting users.");
				return false;
			}

			if (($sheet = $this->model->get_sheet($sheet_id)) === false) {
				$this->view->add_tag("result", "Error getting sheet.");
				return false;
			}

			$args = array(
				"sheet_id" => $sheet_id,
				"currency" => $sheet["currency"]);
			$this->view->open_tag("edit", $args);

			$this->view->record($payment, "payment");

			$this->view->open_tag("users");
			foreach ($users as $user) {
				$args = array(
					"id"   => $user["id"],
					"part" => $payment["part"][$user["id"]]);
				$this->view->add_tag("user", $user["fullname"], $args);
			}
			$this->view->close_tag();

			$this->view->open_tag("part");
			for ($p = 0; $p <= $this->settings->max_payment_part; $p++) {
				$this->view->add_tag("value", $p);
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$url = array("url" => "sheet");

			if (($sheet_id = $this->page->pathinfo[1]) == null) {
				$this->view->add_tag("title", "Payments");
				$this->view->add_tag("result", "No balance sheet specified.", $url);
				return;
			}

			if (($sheet = $this->model->get_sheet($sheet_id)) == false) {
				$this->view->add_tag("title", "Payments");
				$this->view->add_tag("result", "Invalid balance sheet specified.", $url);
				return;
			}

			$this->view->add_tag("title", $sheet["title"]);

			if (is_true($sheet["locked"])) {
				$this->view->add_tag("result", "This sheet has been locked.", $url);
				return;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$_POST["amount"] = str_replace(",", ".", $_POST["amount"]);

				if ($_POST["submit_button"] == "Save payment") {
					/* Save payment
					 */
					if ($this->model->save_oke($sheet_id, $_POST) == false) {
						$this->show_payment_form($sheet_id, $_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create payment
						 */
						if ($this->model->create_payment($sheet_id, $_POST) === false) {
							$this->view->add_message("Error creating payment.");
							$this->show_payment_form($sheet_id, $_POST);
						} else {
							$this->user->log_action("payment %d created", $this->db->last_insert_id);
							$this->show_overview($sheet);
						}
					} else {
						/* Update payment
						 */
						if ($this->model->update_payment($sheet_id, $_POST) === false) {
							$this->view->add_message("Error updating payment.");
							$this->show_payment_form($sheet_id, $_POST);
						} else {
							$this->user->log_action("payment %d updated", $_POST["id"]);
							$this->show_overview($sheet);
						}
					}
				} else if ($_POST["submit_button"] == "Delete payment") {
					/* Delete payment
					 */
					if ($this->model->delete_oke($sheet_id, $_POST["id"]) == false) {
						$this->show_payment_form($sheet_id, $_POST);
					} else if ($this->model->delete_payment($_POST["id"]) === false) {
						$this->view->add_message("Error deleting payment.");
						$this->show_payment_form($sheet_id, $_POST);
					} else {
						$this->user->log_action("payment %d deleted", $_POST["id"]);
						$this->show_overview($sheet);
					}
				} else {
					$this->show_overview($sheet);
				}
			} else if ($this->page->pathinfo[2] === "new") {
				/* New payment
				 */
				$payment = array("part" => array());
				if (($users = $this->model->get_participants($sheet_id)) !== false) {
					foreach ($users as $user) {
						$payment["part"][$user["id"]] = 1;
					}
				}
				$this->show_payment_form($sheet_id, $payment);
			} else if (valid_input($this->page->pathinfo[2], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit payment
				 */
				if (($payment = $this->model->get_payment($sheet_id, $this->page->pathinfo[2])) === false) {
					$this->view->add_tag("result", "Payment not found.");
				} else {
					$this->show_payment_form($sheet_id, $payment);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview($sheet);
			}
		}
	}
?>
