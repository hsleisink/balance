<?php
	class sheet_controller extends Banshee\controller {
		private function show_overview() {
			if (($sheet_count = $this->model->count_sheets()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$paging = new Banshee\pagination($this->view, "sheets", $this->settings->admin_page_size, $sheet_count);

			if (($sheets = $this->model->get_sheets($paging->offset, $paging->size)) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("sheets");
			foreach ($sheets as $sheet) {
				$sheet["date"] = date("j F Y", strtotime($sheet["date"]));
				$sheet["locked"] = show_boolean($sheet["locked"]);
				$this->view->record($sheet, "sheet");
			}
			$this->view->close_tag();

			$paging->show_browse_links();

			$this->view->close_tag();
		}

		private function show_sheet_form($sheet) {
			if (($users = $this->model->get_users()) === false) {
				$this->view->add_tag("result", "Database error.");
				return false;
			}

			$this->view->add_css("jquery/jquery-ui.css");
			$this->view->add_javascript("jquery/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");
			$this->view->add_help_button();

			if (isset($sheet["id"])) {
				$this->view->add_system_warning("Be careful! Removing participants can affect the balance of this sheet.");
			}

			$this->view->open_tag("edit", array("previous" => $this->page->previous));

			$sheet["locked"] = show_boolean($sheet["locked"] ?? null);
			$this->view->record($sheet, "sheet");

			$this->view->open_tag("users");
			foreach ($users as $user) {
				$param = array(
					"id"       => $user["id"],
					"selected" => show_boolean(in_array($user["id"], $sheet["participants"])));
				$this->view->add_tag("user", $user["fullname"], $param);
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function get_user($name) {
			$this->view->open_tag("result");

			if (($user = $this->model->get_user($name)) == false) {
				$this->view->add_tag("error", "User not found.");
			} else {
				$this->view->add_tag("user", $user["fullname"], array("id" => $user["id"]));
			}

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save sheet") {
					$this->model->fix_participants($_POST);
					/* Save sheet
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_sheet_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create sheet
						 */
						if ($this->model->create_sheet($_POST) === false) {
							$this->view->add_message("Error creating sheet.");
							$this->show_sheet_form($_POST);
						} else {
							$this->user->log_action("sheet %d created", $this->db->last_insert_id);
							$this->model->notify_users($_POST);
							$this->show_overview();
						}
					} else {
						/* Update sheet
						 */
						if ($this->model->update_sheet($_POST) === false) {
							$this->view->add_message("Error updating sheet.");
							$this->show_sheet_form($_POST);
						} else {
							$this->user->log_action("sheet %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete sheet") {
					/* Delete sheet
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_sheet_form($_POST);
					} else if ($this->model->delete_sheet($_POST["id"]) === false) {
						$this->view->add_message("Error deleting sheet.");
						$this->show_sheet_form($_POST);
					} else {
						$this->user->log_action("sheet %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if (($this->page->pathinfo[1] ?? null) === "new") {
				/* New sheet
				 */
				if (($users = $this->model->get_users()) === false) {
					$this->view->add_tag("result", "Database error.");
				} else {
					$sheet = array(
						"date"         => date("Y-m-d"),
						"currency"     => EURO,
						"participants" => array());
					foreach ($users as $user) {
						if (($user["addressbook_id"] ?? null) === null) {
							array_push($sheet["participants"], (int)$user["id"]);
						}
					}
					$this->show_sheet_form($sheet);
				}
			} else if (valid_input($this->page->pathinfo[1] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit sheet
				 */
				if (($sheet = $this->model->get_sheet($this->page->pathinfo[1])) === false) {
					$this->view->add_tag("result", "Sheet not found.");
				} else if ($sheet["user_id"] != $this->user->id) {
					$this->view->add_tag("result", "You are not allowed to edit this sheet.");
					$this->user->log_action("unauthorized edit attempt for sheet %d", $sheet["id"]);
				} else {
					$this->show_sheet_form($sheet);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
