<?php
	class balance_controller extends Banshee\controller {
		/* Show balance
		 */
		private function show_balance($sheet, $users, $payments) {
			$this->view->title = $sheet["title"];

			$args = array(
				"id"       => $sheet["id"],
				"previous" => $this->page->previous,
				"edit_ss"  => show_boolean($sheet["user_id"] == $this->user->id),
				"locked"   => show_boolean($sheet["locked"]));
			$this->view->open_tag("sheet", $args);

			$this->view->open_tag("users");
			foreach ($users as $user) {
				$this->view->add_tag("user", $user["fullname"]);
			}
			$this->view->close_tag();

			$total = array(0);

			$this->view->open_tag("payments", array("currency" => $sheet["currency"]));
			foreach ($payments as $payment) {
				$total[0] += $payment["amount"];

				$part_sum = 0;
				foreach ($users as $user) {
					$part_sum += $payment["parts"][$user["id"]] ?? 0;
				}

				$this->view->open_tag("payment");

				$this->view->record($payment);

				$this->view->open_tag("parts");
				foreach ($users as $user) {
					$part = $payment["parts"][$user["id"]] ?? 0;

					if ($part_sum == 0) {
						$amount = 0;
					} else {
						$amount = ($payment["amount"] / $part_sum) * $part;

						if ($user["id"] == $payment["payer_id"]) {
							$amount = $payment["amount"] - $amount;
							$type = ($amount > 0) ? "pos" : "neg";

							$total[$user["id"]] = $total[$user["id"]] ?? 0 + $amount;
						} else {
							$type = ($amount > 0) ? "neg" : "pos";

							$total[$user["id"]] = $total[$user["id"]] ?? 0 - $amount;
						}
					}

					if ($amount == 0) {
						$type = "nul";
					} else if ($amount < 0) {
						$amount = -$amount;
					}

					$amount = sprintf("%.2f", $amount);
					$this->view->add_tag("part", $amount, array("type" => $type));
				}
				$this->view->close_tag();

				$this->view->close_tag();
			}
			$this->view->close_tag();

			foreach ($total as $i => $amount) {
				$total[$i] = sprintf("%.2f", $amount);
			}

			$this->view->open_tag("total", array("amount" => $total[0]));
			foreach ($users as $user) {
				$amount = $total[$user["id"]];
				if ($amount > 0) {
					$type = "pos";
				} else if ($amount < 0) {
					$amount = -$amount;
					$type = "neg";
				} else {
					$type = "nul";
				}

				$this->view->add_tag("amount", sprintf("%.2f", $amount), array("type" => $type));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		/* Export balance to CSV
		 */
		private function export_balance($sheet, $users, $payments) {
			$csv = new Banshee\csvfile();

			$csv->add_line($sheet["title"]);
			$csv->add_line();

			$line = array("Title", "Amount", "Payer");
			foreach ($users as $user) {
				array_push($line, $user["fullname"]);
			}
			$csv->add_line($line);

			foreach ($payments as $payment) {
				$total[0] += $payment["amount"];

				$part_sum = 0;
				foreach ($users as $user) {
					$part_sum += $payment["parts"][$user["id"]];
				}

				$line = array($payment["title"], $payment["amount"], $payment["fullname"]);

				foreach ($users as $user) {
					$part = $payment["parts"][$user["id"]];
					$amount = ($payment["amount"] / $part_sum) * $part;
					if ($user["id"] == $payment["payer_id"]) {
						$amount = $payment["amount"] - $amount;
						$type = "";

						$total[$user["id"]] += $amount;
					} else {
						$total[$user["id"]] -= $amount;

						if ($amount > 0) {
							$type = "-";
						} else {
							$amount = -$amount;
						}
					}

					if ($amount == 0) {
						$type = "";
					}

					$amount = sprintf("%.2f", $amount);
					array_push($line, $type.$amount);
				}

				$csv->add_line($line);
			}

			foreach ($total as $i => $amount) {
				$total[$i] = sprintf("%.2f", $amount);
			}

			$line = array("Total", $total[0], "");
			foreach ($users as $user) {
				$amount = $total[$user["id"]];
				if ($amount > 0) {
					$type = "";
				} else if ($amount < 0) {
					$amount = -$amount;
					$type = "-";
				} else {
					$type = "";
				}

				array_push($line, sprintf("%s%.2f", $type, $amount));
			}

			$csv->add_line($line);

			$this->view->disable();
			header("Content-Type: text/csv");
			header("Content-Disposition: attachment; filename=\"sheet_".$sheet["id"].".csv\"");
			print $csv->to_string();
		}

		/* Execute
		 */
		public function execute() {
			$this->view->title = "Balance Sheet";

			$url = array("url" => "sheet");

			if (($sheet_id = $this->page->pathinfo[1]) == null) {
				$this->view->add_tag("result", "No balance sheet specified.", $url);
				return;
			}

			if (($sheet = $this->model->get_sheet($sheet_id)) == false) {
				$this->view->add_tag("result", "Invalid balance sheet specified.", $url);
				return;
			}

			if (($users = $this->model->get_participants($sheet_id)) === false) {
				$this->view->add_tag("result", "Error getting users.", $url);
				return false;
			}

			if (($payments = $this->model->get_payments($sheet_id)) === false) {
				$this->view->add_tag("result", "Error getting payments.", $url);
				return false;
			}

			if (($this->page->pathinfo[2] ?? null) == "export") {
				$this->export_balance($sheet, $users, $payments);
			} else {
				$this->show_balance($sheet, $users, $payments);
			}
		}
	}
?>
